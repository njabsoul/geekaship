import javax.swing.*;

/**
 * Created by Njabz The Programmer on 10/8/2015.
 */
public class PigLatinMain {

    public static void main(String[] args) {

        String msg ="Select 1 to translate from English to Pif Latin \n" +
                    "Select 2 to Translate from Pig LAtin to English";
        String input;
        int option = Integer.parseInt(JOptionPane.showInputDialog(null,msg));
        String answer;
       switch (option){

           case 1:
                 input= JOptionPane.showInputDialog(null,"Enter the English word to be translated");
                 answer=translateToPigLatin(input);
               System.out.println(answer);
               break;
           case 2:
               input= JOptionPane.showInputDialog(null,"Enter the English word to be translated");
               answer=translateToEnglish(input);
               System.out.println(answer);
               break;


       }

    }

    private static String translateToEnglish(String input) {
        String pigTrans="";

        if(input.endsWith("-way")){
            pigTrans=input.substring(0,input.indexOf("-way"));
        }
        else{
            pigTrans =input.substring(input.indexOf('-')+1,input.indexOf("ay"))+input.substring(0,input.indexOf('-'));
        }

        return pigTrans;
    }

    private static String translateToPigLatin(String input) {
        String pigTrans="";
        input=input.toLowerCase();

        if (input.startsWith("a")||input.startsWith("e")||input.startsWith("i")||input.startsWith("o")||input.startsWith("u")){

            pigTrans = input+"-way";
        }
        else{
            int index=0;
            String cons ="";
            for (int i = 0; i <input.length() ; i++) {

                if(!(input.charAt(i)=='a'||input.charAt(i)=='e'||input.charAt(i)=='i'||input.charAt(i)=='o'||input.charAt(i)=='u')){

                    index++;
                }
                else {
                    i=input.length()+1;
                }
            }

            cons =input.substring(0,index);
            pigTrans = input.substring(cons.length())+"-"+cons+"ay";
        }


        return pigTrans;
    }


}
