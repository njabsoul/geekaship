/**
 * Created by Njabz The Programmer on 10/6/2015.
 */
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
public class StackFormulaBalance {

    Map<Character, Character> brackets = new HashMap<Character, Character>() {{
        put('{', '}');
        put('(', ')');
        put('[', ']');
    }};

    public boolean balance(String toBalance) {
        Deque<Character> bracketsStack = new ArrayDeque<Character>();
        for (int i = 0; i < toBalance.length(); i++) {
            char currentChar = toBalance.charAt(i);
            if (characterIsOpenBracket(currentChar)) {
                bracketsStack.push(currentChar);
            } else if (characterIsAClosingBracket(currentChar) &&
                    closingBracketMatchesLastOpeningBracket(bracketsStack, currentChar)) {
                return false;
            }
        }
        return bracketsStack.isEmpty();
    }

    private boolean closingBracketMatchesLastOpeningBracket(Deque<Character> bracketsStack, char currentChar) {
        return bracketsStack.size() == 0 || !brackets.get(bracketsStack.pop()).equals(currentChar);
    }

    private boolean characterIsAClosingBracket(char currentChar) {
        return brackets.values().contains(currentChar);
    }

    private boolean characterIsOpenBracket(char currentChar) {
        return brackets.containsKey(currentChar);
    }
}
