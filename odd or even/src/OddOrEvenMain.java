import java.util.Random;

/**
 * Created by Njabz The Programmer on 9/18/2015.
 */
public class OddOrEvenMain {

    public static void main(String[] args) {
        int[] numbers=new int[10];
        Random ran = new Random();

        for (int i = 0; i <numbers.length ; i++) {
            numbers[i] = ran.nextInt(100);

        }
        //checking the remainder using modulus
        for (int i = 0; i <numbers.length ; i++) {

            if(numbers[i]%2==0){
                System.out.println(""+numbers[i]+" is an even number");
            }
            else{
                System.out.println(""+numbers[i]+" is an odd number");
            }

        }


        //checking if even using bitwise

        System.out.println("cheking using bitsiwe");
        for (int i = 0; i <numbers.length ; i++) {

            if(isEven(numbers[i])){
                System.out.println(""+numbers[i]+" is an even number");
            }
            else{
                System.out.println(""+numbers[i]+" is an odd number");
            }

        }


    }

    public static boolean isEven(int i){
        return (i % 2) == 0;
    }
}



