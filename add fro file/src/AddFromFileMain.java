import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Njabz The Programmer on 9/21/2015.
 */
public class AddFromFileMain {

    public static void main(String[] args) {
        try {

            addNumbers();
        }
        catch (IOException e){

        }
    }


    public static void addNumbers()throws IOException{
     int sum =0;

        String output="";
        int row = countRows();
        FileReader fileReader = new FileReader("numbers.txt");
        BufferedReader br = new BufferedReader(fileReader);
        for (int i = 0; i <row ; i++) {

            int number = Integer.parseInt(br.readLine());
            output =output+ number+" + ";

            sum+=number;
        }
       String i= output.substring(0,output.length()-3);
        i = i+" = " +sum;

        System.out.println(i);

    }

    public static int countRows()throws IOException{
        int rows=0;

        FileReader fileReader = new FileReader("numbers.txt");
        BufferedReader br = new BufferedReader(fileReader);

        while (br.readLine()!=null){
            rows++;
        }


        return rows;
    }
}
