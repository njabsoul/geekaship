import javax.swing.*;

/**
 * Created by Njabz The Programmer on 10/12/2015.
 */
public class BruteForceStringSearchMain {
    public static void main(String[] args) {

        String pattern = JOptionPane.showInputDialog(null,"Please enter the pattern");
        String text = JOptionPane.showInputDialog(null,"Please enter the text");


         int theINdex = searchAndDisplay(pattern,text);
        if(theINdex==-1){
            System.out.println("the pattern was not found");
        }
        else {
            System.out.println("the pattern was found at index :" +theINdex );
        }



    }

    public static int searchAndDisplay(String patt, String text){

            for (int i = 0; i <text.length()-patt.length() ; i++) {

                int index =0;

                while (index < patt.length() && text.charAt(i + index)== patt.charAt(index)) {
                    index++;
                }
                if (index == patt.length()) return i;


            }



       return -1;
    }
}
