import java.io.File;

/**
 * Created by Njabz The Programmer on 10/6/2015.
 */
public class WalkingDirectoryMain {

    public static void main(String[] args) {
        File folder = new File("C:\\Users\\Njabz The Programmer\\Documents");
      //  System.out.println(folder.isDirectory());
        listFilesForFolder(folder);
    }

    public static void listFilesForFolder( File folder) {

        if (folder.isDirectory()){

            for (int i = 0; i < folder.listFiles().length; i++) {

                if (folder.listFiles()[i].getPath().endsWith(".txt")){
                    System.out.println(folder.listFiles()[i].getPath());
                }


            }

        }
    }

}
