import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Njabz The Programmer on 9/17/2015.
 */
public class AnagramMain {

    public static void main(String[] args){

        AnagramBackEndFunction anagram = new AnagramBackEndFunction();
        ArrayList<String> wordsToPrint= new ArrayList<String>();
        try{
            String[] wordFromTheFile = anagram.getWordsFromTheFile();

           /* for (int i = 0; i <wordFromTheFile.length ; i++) {
                System.out.println(wordFromTheFile[i]);
            }*/

            String input = JOptionPane.showInputDialog(null,"please enter the word");

            wordsToPrint = anagram.getAnagram(input,wordFromTheFile);
            ArrayList<String> theAnagram = loadAnagram(input,wordsToPrint);


            for (int i = 0; i <wordsToPrint.size() ; i++) {
                System.out.print(theAnagram.get(i)+"\t");
            }
        }
        catch (Exception e){

        }

    }

    public  static ArrayList<String> loadAnagram(String input, ArrayList<String> posibleWords){

        ArrayList<String> theAanagram = new ArrayList<String>();
        AnagramBackEndFunction anagram = new AnagramBackEndFunction();

        for (int i = 0; i <posibleWords.size() ; i++) {

            if(anagram.isAnAnagram(input,posibleWords.get(i))){
                theAanagram.add(posibleWords.get(i));
            }
        }

        return theAanagram;
    }

}
