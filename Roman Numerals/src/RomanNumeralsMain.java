import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Njabz The Programmer on 10/6/2015.
 */
public class RomanNumeralsMain {

    public static void main(String[] args) {
        String romanInput1 = JOptionPane.showInputDialog(null,"Please enter a number in roman numerals");
        String romanInput2 = JOptionPane.showInputDialog(null,"Please enter a number in roman numerals");


        System.out.println("The first number is "+ getIntVal(romanInput1));
        System.out.println("The first number is "+ getIntVal(romanInput2));

        System.out.println("The sum of the two numbers is = "+ add_roman(romanInput1,romanInput2));

    }

    public static int add_roman(String input1, String input2){
        int answer=0;

        int val1=getIntVal(input1);
        int val2=getIntVal(input2);



        return val1+val2;
    }

    public static int getIntVal(String roman){
        int val=0;
        ArrayList<Integer> values = new ArrayList<>();
        for (int i = 0; i <roman.length() ; i++) {

            if(roman.charAt(i)=='M'){
              values.add(1000);
            }
            else if(roman.charAt(i)=='D'){
                values.add(500);
            }
            else if(roman.charAt(i)=='C'){
                values.add(100);
            }
            else if(roman.charAt(i)=='L'){
                values.add(50);
            }
            else if(roman.charAt(i)=='X'){
                values.add(10);
            }
            else if(roman.charAt(i)=='V'){
                values.add(5);
            }
            else if(roman.charAt(i)=='I'){
                values.add(1);
            }

        }

        for (int i = 0; i <values.size()-1 ; i++) {
            if(values.get(i)<values.get(i+1)){
                val +=values.get(i+1)-values.get(i);
                i++;
            }
            else{
                val+= values.get(i);
            }
        }
        return  val;
    }

}
