import java.util.Random;

/**
 * Created by Njabz The Programmer on 9/21/2015.
 */
public class SumMain {

    public static void main(String[] args) {
        int[] numbers= new int[10];
        Random ran = new Random();
        for(int i = 0; i <numbers.length ; i++) {

            numbers[i]=ran.nextInt(101);
        }
        int sum = 0;
        for (int i = 0; i <numbers.length ; i++) {
            System.out.print("" + numbers[i] + ", ");

                sum+=numbers[i];

        }

        System.out.println("");
        System.out.println("The sum is = "+ sum);

    }


}
