import javax.swing.*;

/**
 * Created by Njabz The Programmer on 9/22/2015.
 */
public class MorseCodeMain {

    public static void main(String[] args) {

        String input =JOptionPane.showInputDialog(null,"please enter the massege you want to encode");
        //String output = decodeFromMorseCode(input);
       // decodeFromMorseCode(input);
      //System.out.println(decodeFromMorseCode(input));
       System.out.println(encodeToMorseCode(input));
        //encodeToMorseCode


    }


       // String rgx ="\\b(?:addAction|intentFilter)\\b\\s*\\(\"\\s*word\\s*\"\\)";

       /* if(rgx.matches(input)){
            System.out.println("true");
        }
        else{
            System.out.println("false");
        }*/
       // System.out8.println("addAction(\"word\")".matches(rgx));
       // System.out.println("intentFilter(\"word\")".matches(rgx));



    public static String decodeFromMorseCode(String input){
        String morseString="";

        input=input.toUpperCase();

        
        input = input.replace("A", ". -  ");
        input =input.replace("B", "- . . .  ");
        input =input.replace("C", "- . - .  ");
        input =input.replace("D", "- . .  ");
        input =input.replace("E", ".  ");
        input =input.replace("F", ". . - .  ");
        input =input.replace("G", "- - .  ");
        input = input.replace("H", ". . . .  ");
        input = input.replace("I", ". .  ");
        input = input.replace("J", ". - - -  ");
        input = input.replace("K", "- . -  ");
        input =input.replace("L", ". - . .  ");
        input =input.replace("M", "- -  ");
        input =input.replace("N", "- .  ");
        input = input.replace("O", "- - -  ");
        input = input.replace("P", ". - - .  ");
        input =input.replace("Q", "- - . -  ");
        input = input.replace("R", ". - .  ");
        input =input.replace("S", ". . .  ");
        input = input.replace("T", "-  ");
        input = input.replace("U", ". . -  ");
        input =input.replace("V", ". . . -  ");
        input =input.replace("W", ". - -  ");
        input =  input.replace("X", "- . . -  ");
        input =  input.replace("Y", "- . - -  ");
        input = input.replace("Z", "- - . .  ");
        input = input.replace("1", ". - - - -  ");
        input = input.replace("2", ". . - - -  ");
        input = input.replace("3", ". . . - -  ");
        input = input.replace("4", ". . . . -  ");
        input = input.replace("5", ". . . . .  ");
        input = input.replace("6", "- . . . .  ");
        input = input.replace("7", "- - . . .  ");
        input = input.replace("8", "- - - . .  ");
        input =input.replace("9", "- - - - .  ");
        input = input.replace("0", "- - - - -  ");
        input =input.replace(" ", "  ");

        return input;

    }
    public static String encodeToMorseCode(String input){
        String morseString="";

        input=input.toUpperCase();
        String[] arrayOfMorse=input.split("  ");

        for (int i = 0; i <arrayOfMorse.length -1; i++){

            if(arrayOfMorse[i].equalsIgnoreCase(". -")){
                morseString=morseString+"A";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- . . .")){
                morseString=morseString+"B";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- . - .")){
                morseString=morseString+"C";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- . .")){
                morseString=morseString+"D";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(".")){
                morseString=morseString+"E";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". . - .")){
                morseString=morseString+"F";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- - .")){
                morseString=morseString+"G";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". . . .")){
                morseString=morseString+"H";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". .")){
                morseString=morseString+"I";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". - - -")){
                morseString=morseString+"J";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- . -")){
                morseString=morseString+"K";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". - . .")){
                morseString=morseString+"L";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- -")){
                morseString=morseString+"M";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- .")){
                morseString=morseString+"N";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- - -")){
                morseString=morseString+"O";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". - - .")){
                morseString=morseString+"P";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- - . -")){
                morseString=morseString+"Q";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". - .")){
                morseString=morseString+"R";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". . .")){
                morseString=morseString+"S";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("-")){
                morseString=morseString+"T";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". . -")){
                morseString=morseString+"U";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". . . -")){
                morseString=morseString+"V";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". - -")){
                morseString=morseString+"W";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- . . -")){
                morseString=morseString+"X";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- . - -")){
                morseString=morseString+"Y";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- - . .")){
                morseString=morseString+"Z";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". - - - -")){
                morseString=morseString+"1";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". . - - -")){
                morseString=morseString+"2";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". . . - -")){
                morseString=morseString+"3";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". . . . -")){
                morseString=morseString+"4";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(". . . . .")){
                morseString=morseString+"5";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- . . . .")){
                morseString=morseString+"6";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- - . . .")){
                morseString=morseString+"7";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- - - . .")){
                morseString=morseString+"8";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- - - - .")){
                morseString=morseString+"9";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase("- - - - -")){
                morseString=morseString+"0";
            }
            else if(arrayOfMorse[i].equalsIgnoreCase(" ")){
                morseString=morseString+" ";
            }


        }

/*
        input = input.replace(". - - - -", "1");
        input = input.replace(". . - - -", "2");
        input = input.replace(". . . - -", "3");
        input = input.replace(". . . . -", "4");
        input = input.replace(". . . . .", " 5");
        input = input.replace("- . . . .", "6");
        input = input.replace("- - . . .", "7");
        input = input.replace("- - - . .", "8");
        input =input.replace("- - - - .", "9");
        input = input.replace("- - - - -", "0");
        input =input.replace("  ", " ");*/
        return morseString;

    }



}
