/**
 * Created by Njabz The Programmer on 10/6/2015.
 */
public class PancakeFlipMain {

    public static void main(String[] args) {

        int[] pancakeSizes = new int[]{49,54,5,61,45,65,15,85,45,48,8,6};

        sort(pancakeSizes);

        for (int i = 0; i <pancakeSizes.length ; i++) {
            System.out.println(pancakeSizes[i]);
        }


    }

    public static  void sort(int[] sizes){
        int temp;
        for (int i = 0; i <sizes.length-1 ; i++) {

            for (int j = 0; j <sizes.length-1 ; j++) {

                if(sizes[j]<sizes[j+1]){
                    temp = sizes[j];
                    sizes[j]=sizes[j+1];
                    sizes[j+1]=temp;
                }
            }
        }
    }
}
