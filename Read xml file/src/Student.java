/**
 * Created by Njabz The Programmer on 9/21/2015.
 */
public class Student {

    private String firstname;
    private String gender;
    private String dateOfBirth;
    private Pet per;

    public Student() {
    }

    public Student(String firstname, String gender, String dateOfBirth, Pet pet) {
        this.firstname = firstname;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
        this.per=pet;
    }

    public Pet getPer() {
        return per;
    }

    public void setPer(Pet per) {
        this.per = per;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}
