import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Njabz The Programmer on 9/21/2015.
 */
public class DelefileMain {

    public static void main(String[] args) {

        try{

            deleteFile();
            deleteDir();

        }
        catch (IOException io){

        }


    }

    public static void deleteFile()throws IOException{
        File file = new File("docs\\input.txt");

        if(file.exists()){
            if(file.delete())
                System.out.println("File is Deleted");;
        }
    }

    public static void deleteDir() throws IOException{

        File dir = new File("docs");
        if(dir.exists()){
            if(dir.delete()){
                System.out.println("Folder is deleted");
            }
        }
    }
}
