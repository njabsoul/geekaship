import java.util.Random;

/**
 * Created by Njabz The Programmer on 9/21/2015.
 */
public class FindMaxMain {

    public static void main(String[] args) {

        int[] numbers= new int[10];
        Random ran = new Random();
        for (int i = 0; i <numbers.length ; i++) {

            numbers[i]=ran.nextInt(101);
        }
        int max = 0;
        for (int i = 0; i <numbers.length ; i++) {
            System.out.print("" + numbers[i] + ", ");
            if(numbers[i]>max){
                max=numbers[i];
            }
        }

        System.out.println("");
        System.out.println("The maximum is = "+ max);




    }
}
