/**
 * Created by Njabz The Programmer on 9/21/2015.
 */
public class TwelveTimeMain {

    public static void main(String[] args) {

        for (int i = 1; i <13 ; i++) {

            for (int j = 1; j <13 ; j++) {
                int product = i*j;
                System.out.println(""+i+" X "+j+" = "+product);
            }
        }
    }
}
