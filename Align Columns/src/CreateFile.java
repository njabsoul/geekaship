import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfAppearance;
import com.lowagie.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;

/**
 * Created by Njabz The Programmer on 9/17/2015.
 */
public class CreateFile {

    public  CreateFile(){

    }

    public CreateFile(String fileName, Object[] arrayOfArrays, String alignmentType) throws Exception{


        Document document = new Document();

        File file = new File(fileName);

        OutputStream outST=new FileOutputStream(file.getAbsoluteFile());




            PdfWriter.getInstance(document, outST);
            file.mkdir();



          document.open();



        String[] dataInfo1=(String[])arrayOfArrays[0];
        String[] dataInfo2=(String[])arrayOfArrays[1];
        String[] dataInfo3=(String[])arrayOfArrays[2];
        String[] dataInfo4=(String[])arrayOfArrays[3];
        String[] dataInfo5=(String[])arrayOfArrays[4];





        Table tableLeft = new Table(5, dataInfo1.length);
        Table tableRight = new Table(5, dataInfo1.length);
        Table tableCenter = new Table(5, dataInfo1.length);

        tableLeft.setSpacing(1);
        tableRight.setSpacing(1);
        tableCenter.setSpacing(1);



        //start adding infor for left
        addToTable(tableLeft, dataInfo1, "left");
        addToTable(tableLeft,dataInfo2,"left");
        addToTable(tableLeft, dataInfo3, "left");
        addToTable(tableLeft, dataInfo4, "left");
        addToTable(tableLeft, dataInfo5, "left");

        //end adding infor for left

        //start adding infor for Right
        addToTable(tableRight,dataInfo1,"right");
        addToTable(tableRight,dataInfo2,"right");
        addToTable(tableRight,dataInfo3,"right");
        addToTable(tableRight,dataInfo4,"right");
        addToTable(tableRight,dataInfo5,"right");

        //end adding infor for Right

        //start adding infor for Center
        addToTable(tableCenter,dataInfo1,"center");
        addToTable(tableCenter,dataInfo2,"center");
        addToTable(tableCenter, dataInfo3, "center");
        addToTable(tableCenter, dataInfo4, "center");
        addToTable(tableCenter, dataInfo5, "center");

        //end adding infor for Center

        document.add(tableLeft);
        document.add(tableRight);
        document.add(tableCenter);

        document.close();

    }

    public void addToTable(Table table, String[] data, String alignmentType){

        for (int i = 0; i <data.length ; i++) {

            Cell cell = new Cell(data[i]);


            if(alignmentType.equalsIgnoreCase("right")){

                cell.setHorizontalAlignment(PdfAppearance.ALIGN_RIGHT);

            }
            else if(alignmentType.equalsIgnoreCase("center")){

                cell.setHorizontalAlignment(PdfAppearance.ALIGN_CENTER);

            }

            table.addCell(cell);

        }


    }
}
