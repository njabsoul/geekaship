/**
 * Created by Njabz The Programmer on 9/17/2015.
 */
public class BottlesMain {

    public static void main(String[] args){

        String first_line=" bottles of beer on the wall";
        String second_line=" bottles of beer";
        int number_of_bottles_left=99;

        for (int i = 0; i <100 ; i++) {

            System.out.println(""+number_of_bottles_left+ first_line);
            System.out.println(""+number_of_bottles_left+ second_line);

            number_of_bottles_left--;
        }
    }
}
