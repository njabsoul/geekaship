import javax.swing.*;
import java.util.Vector;

/**
 * Created by Njabz The Programmer on 10/12/2015.
 */
public class GoldbachsConjectureMain {

    public static void main(String[] args) {

        int chosenEvemNumber=Integer.parseInt(JOptionPane.showInputDialog(null,"Please enter an even nunber greater than 2"));
        Vector<Integer> primes = new Vector<Integer>();
        System.out.println("Prime numbers between 2 and "+chosenEvemNumber);
        System.out.println("-------------------------------------");
        for (int i = 1; i <chosenEvemNumber; i++) {
            if(isPrime(i)){
                primes.add(i);
                System.out.println(i);
            }
        }
        System.out.println("-------------------------------------");

        for (int i = 0; i <primes.size()-1 ; i++) {
                for (int j = 0; j <primes.size()-1 ; j++) {
                    int sumOfPrime =primes.get(i)+primes.get(j);
                    if(sumOfPrime==chosenEvemNumber){

                        System.out.println("the prime numbers that make up "+chosenEvemNumber+" are "+primes.get(i)+" and "+primes.get(j));

                        j=primes.size()+1;
                        i=primes.size()+1;
                    }
                }
        }


    }

   public static boolean isPrime(int n) {
        for(int i=2;i<n;i++) {
            if(n%i==0)

                return false;
        }
        return true;
    }
}
